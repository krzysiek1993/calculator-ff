package eu.kkaczynski.calculator.model.instruction;

public record Instruction(
        InstructionType instructionType,
        Long instructionOperand) {

    public InstructionType getInstructionType() {
        return instructionType;
    }

    public Long getInstructionOperand() {
        return instructionOperand;
    }

    public boolean isTypeOf(InstructionType type) {
        return this.instructionType == type;
    }
}
