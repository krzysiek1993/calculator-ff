package eu.kkaczynski.calculator.model.instruction;

import java.util.Arrays;
import java.util.Optional;

public enum InstructionType {

    ADD("add"), SUBTRACT("subtract"), MULTIPLY("multiply"), DIVIDE("divide"), APPLY("apply");

    private final String symbol;

    InstructionType(String symbol) {
        this.symbol = symbol;
    }

    public String getSymbol() {
        return symbol;
    }

    public static Optional<InstructionType> fromText(String text) {
        return Arrays.stream(values())
                .filter(bl -> bl.getSymbol().equals(text))
                .findFirst();
    }
}