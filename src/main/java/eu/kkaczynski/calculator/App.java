package eu.kkaczynski.calculator;

import eu.kkaczynski.calculator.service.InstructionService;

public class App {
    public static void main(String[] args) {
        InstructionService instructionService = new InstructionService();
        System.out.println(instructionService.compute("Example_2.txt"));
    }
}