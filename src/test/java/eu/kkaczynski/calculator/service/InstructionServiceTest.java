package eu.kkaczynski.calculator.service;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.io.IOException;

class InstructionServiceTest {

    @Test
    void testFilesWithData() {
        InstructionService instructionService =  new InstructionService();
        Long example1 = instructionService.compute("Example_1.txt");
        Long example3 = instructionService.compute("Example_3.txt");
        Assertions.assertEquals(36, example1);
        Assertions.assertEquals(1, example3);
    }

    @Test
    void testEmptyFile() {
        InstructionService instructionService =  new InstructionService();
        Assertions.assertThrows(IllegalArgumentException.class, () -> instructionService.compute("EmptyFile.txt"),"cannot find apply");
    }

    @Test
    void testAsNotFoundFile() {
        InstructionService instructionService =  new InstructionService();
        Assertions.assertThrows(IllegalArgumentException.class, () -> instructionService.compute("EmptyFile.txt"),"cannot find file");
    }
    @Test
    void testDivideZero(){
        InstructionService instructionService = new InstructionService();
        Assertions.assertThrows(IllegalArgumentException.class, ()-> instructionService.compute("Example_2.txt"),"never divide zero");
    }
}
