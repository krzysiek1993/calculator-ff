package eu.kkaczynski.calculator.service;


import eu.kkaczynski.calculator.service.file.FileHelper;
import eu.kkaczynski.calculator.model.instruction.Instruction;
import eu.kkaczynski.calculator.service.instruction.InstructionHelper;
import eu.kkaczynski.calculator.model.instruction.InstructionType;

import java.util.List;
import java.util.stream.Collectors;

public class InstructionService {

    private static final String CANNOT_FIND_FILE = "cannot find file";
    private static final String CANNOT_FIND_APPLY = "cannot find apply";


    public Long compute(String fileName) {
        return executeInstructions(loadInstructions(fileName));
    }

    private List<Instruction> loadInstructions(String fileName) {
        FileHelper fileHelper = new FileHelper();
        String path = fileHelper.getPathResourceWithFileName(fileName);
        return fileHelper.getLinesFromPath(path).stream().map(InstructionHelper::parseInstruction).collect(Collectors.toList());
    }

    private Long executeInstructions(List<Instruction> instructions) {
        if (instructions.isEmpty())
            throw new IllegalArgumentException(CANNOT_FIND_FILE);
        Instruction instruction = instructions.get(instructions.size() - 1);
        if (instruction.isTypeOf(InstructionType.APPLY))
            return solveOperations(instructions);
        else
            throw new IllegalArgumentException(CANNOT_FIND_APPLY);
    }

    private Long solveOperations(List<Instruction> instructions) {
        Long result;
        Instruction firstInstruction = instructions.get(instructions.size() - 1);
        result = firstInstruction.getInstructionOperand();
        for (Instruction instruction : instructions) {
            result = solveOperation(result, instruction);
        }
        return result;
    }

    private Long solveOperation(Long result, Instruction instruction) {
        switch (instruction.getInstructionType()) {
            case ADD:
                return result += instruction.getInstructionOperand();
            case SUBTRACT:
                return result -= instruction.getInstructionOperand();
            case DIVIDE:
                if (instruction.getInstructionOperand() == 0)
                    throw new IllegalArgumentException("never divide zero");
                return result = result / instruction.getInstructionOperand();
            case MULTIPLY:
                return result = result * instruction.getInstructionOperand();
        }
        return result;
    }
}