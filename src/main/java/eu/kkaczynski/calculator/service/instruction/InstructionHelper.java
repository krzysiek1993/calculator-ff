package eu.kkaczynski.calculator.service.instruction;

import eu.kkaczynski.calculator.model.instruction.Instruction;
import eu.kkaczynski.calculator.model.instruction.InstructionType;

import java.util.Optional;

public class InstructionHelper {

    private static final String NOT_SUPPORTED_INSTRUCTION = "Not supported instruction";
    private static final String ONLY_NUMBERS = "[^0-9]";
    private static final String ONLY_LETTERS = "[^A-Za-z]+";

    public static Instruction parseInstruction(String instructionLine) {
        final String operation = parseInstructionName(instructionLine);
        final Long value = parseInstructionOperand(instructionLine);
        Optional<InstructionType> instructionType = InstructionType.fromText(operation);
        if (instructionType.isEmpty())
            throw new IllegalArgumentException(NOT_SUPPORTED_INSTRUCTION);
        else
            return new Instruction(instructionType.get(), value);
    }

    private static String parseInstructionName(String string) {
        return string.replaceAll(ONLY_LETTERS, "");
    }

    private static Long parseInstructionOperand(String instructionLine) {
        return Long.valueOf(instructionLine.replaceAll(ONLY_NUMBERS, ""));
    }
}
