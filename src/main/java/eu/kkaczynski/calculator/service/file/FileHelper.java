package eu.kkaczynski.calculator.service.file;

import eu.kkaczynski.calculator.service.InstructionService;

import java.io.IOException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

public class FileHelper {

    private static final String CANNOT_FIND_FILE = "cannot find file";

    public String getPathResourceWithFileName(String name) {
        URL resource = InstructionService.class.getClassLoader().getResource(name);
        if (resource == null)
            throw new IllegalArgumentException(CANNOT_FIND_FILE);
        String path = Objects.requireNonNull(resource).getPath();
        if (path.startsWith("/"))
            path = path.substring(1);
        return path;
    }

    public List<String> getLinesFromPath(String path) {
        try {
            return Files.lines(Path.of(path)).collect(Collectors.toList());
        } catch (IOException e) {
            e.printStackTrace();
        }
        return new ArrayList<>();
    }

}
